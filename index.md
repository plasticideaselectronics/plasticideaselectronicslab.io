---
layout: default
title: News
---

{% for post in site.posts %}
<div markdown="1" class="post">

<div class="heading" markdown="1">
<div class="date" markdown="1">
{{ post.date | date: "%b %-d, %Y" }}
</div>
<div class="title" markdown="1">
# {{ post.title }}
</div>
</div>

<div class="content" markdown="1">
{{ post.content | markdownize }}
</div>

</div>

{% endfor %}
