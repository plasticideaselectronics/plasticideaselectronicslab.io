---
layout: default
title: About
---

#  Plastic Ideas Electronics

Plastic Ideas Electronics is a Portland, OR based music electronics company. We create useful, efficient musical tools for the needs of performing artists working with sound. 

Plastic Ideas was started in the Summer of 2017 by Alex Norman and Scott Goodwin. We wanted to design instruments and processors based on our experience participating in Portland’s noise, techno, house, and experimental arts communities. Our focus is on finding solutions for electronic artists and musicians in order to support them in creating novel, mind blowing experiences.

Plastic Ideas Electronics was named as a homage to the legendary Pacific Northwest band Unwound’s 1994 album New Plastic Ideas.


**Alex Norman**

Alex is an engineer who's primary interests are in embedded systems and multimedia, particularly audio and music. He is an active contributor to [Futel](http://futel.net/) and a devout member of and contributor to [The Church of Robotron](http://churchofrobotron.com/). As a developer, he has contributed to several multimedia software projects including: [Data Jockey](http://x37v.info/projects/datajockey/), [scvim](http://x37v.info/projects/scvim/), [Pure Data](http://puredata.info/), and [fluxus](http://www.pawfal.org/fluxus/). Under the name 'xnor', Alex performs with his DIY, mostly CGS, Serge Modular system and DJs, with software that he authored.

**Scott Goodwin**

Scott started building DIY synthesizers in 2008 after returning from a short tour of the East Coast performing with three iPods, a microphone, and a crumpled brown paper bag. Over the years he’s played with the glacially static drone group [Bonus](http://www.discogs.com/artist/623581-Bonus-2), polyrhythmic house duo [Polonaise](http://www.discogs.com/artist/2540248-Polonaise), with “structural techno” band [Operative](http://www.youtube.com/watch?v=6pqWPEtP-AE), and with solo projects in both the techno and experimental music genres. Much of his work uses electronic instruments to explore the physicality of sounds, hearing, and perception. He’s also somewhat obsessed with applying concepts from the late 90s/early Aughts DIY hardcore punk scene to electronic art making.


Branding and web design by [Justin C. Meyers](http://jcmeyers.net/), site by [xnor](http://www.x37v.info/).

